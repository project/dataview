
OVERVIEW

Dataview was created to bring the powerful table creation, sorting, and filtering options of the Views module and make them available for data that is unrelated to the nodes table.

In addition, Dataview currently also allows for the export of the current view as an XML-based spreadsheet, including formatting.

Its purpose, however, is to act as an API, so it must be passed an array that describes the table and its fields from which the data will be pulled.

The module that implements Dataview will need to define a menu item at which the dataview will be displayed, and then pass through that location as part of the data model that lets Dataview know how to pull together the information.

See the API document for more details