<?php

// TODO: build similar export capability for CSV

/* Helper function to 
 *
 */

function _dataview_excel($items, $data_model) {
  // first, remove any hidden fields
  $fields = $data_model['fields'];
  foreach ($fields as $field_name => $field) {
    if (_dataview_is_true($field['hidden'])) {
      unset($fields[$field_name]);
    }
  }
  
  $label_color = ($data_model['label_color']) ? $data_model['label_color'] : 'CCCCCC';
  $set_label = $data_model['title'];
  $field_count = count($fields);
  $row_count = count($items)+2;
  $first_field = array_shift(array_keys($fields));
  $last_field = array_pop(array_keys($fields));
  $last_item = count($data_model['items']) - 1;
  
  // create main table
  
  // Describe any filters that have been applied
  //var_dump($data_model['args']); exit;
  $filters = $data_model['args'];
  unset($filters['order']);
  unset($filters['sort']);
  if (count($filters)) {
    foreach ($filters as $field => $field_val) {
      $field_defs = $data_model['fields'][$field];
      
      // First, first the most descriptive field name
      $field_label = ($field_defs['label']) ? $field_defs['label'] : $field;
      
      // Now, try to find a descriptive version of the value
      if (is_array($field_defs['filter_options'])) {
        $filter_options = $field_defs['filter_options'];
        
        if (array_key_exists($field_val, $filter_options) && !is_array($filter_options[$field_val])) {
          $field_val = $filter_options[$field_val];
        }
        elseif (isset($field_defs['filter_dependant'])) {
          if (isset($args[$field_defs['filter_dependant']]) && is_array($filter_options[$args[$field_defs['filter_dependant']]])) {
            // only check in the appropriate set
            $filter_options = $filter_options[$args[$field_defs['filter_dependant']]];
            
            if (array_key_exists($field_val, $filter_options) && !is_array($filter_options[$field_val])) {
              $field_val = $filter_options[$field_val];
            }
          }
          else {
            // Descriptive value may exist deeper in a nested array
/* TO DO: program second-level search of nested arrays
            foreach ($filter_options as $option_set) {
            }
*/
          }
        }
        
      }
      
      
      $filter_type = $data_model['fields'][$field]['type'];
      
      // check for operator to list
      if (in_array($filter_type, array('float', 'int')) && !empty($data_model['filter_ops'][$field])) {
        $op = $data_model['operators'][$data_model['filter_ops'][$field]];
        
        if (strcmp($data_model['filter_ops'][$field], 'range') != 0) {
          $op_show = $op .' ';
        }
      }

      $filter_heads[] = $field_label .': '. $op_show . check_plain($field_val);
//      $filter_heads[] = t('a @field of @field_val', array('@field' => $field_label, '@field_val' => $field_val));
    }
    $filter_text = t('Results for: @values', array('@values' => implode($filter_heads, '; ')));
    
    $table_header .= "   <Row ss:AutoFitHeight=\"1\">\r";
    $table_header .= "   <Cell ss:StyleID=\"s24\" ss:MergeAcross=\"". ($field_count-1) ."\" ss:Alignment=\"Center\"><Data ss:Type=\"String\">$filter_text</Data><NamedCell ss:Name=\"Print_Area\"/></Cell>\r";
    $table_header .= "   </Row>\r";
    $row_count++;
  }
  
  // add a blank row
  $table_header .= "   <Row ss:AutoFitHeight=\"1\">\r";
  $table_header .= "   <Cell ss:StyleID=\"s22\" ss:MergeAcross=\"". ($field_count-1) ."\" ss:Alignment=\"Center\"><Data ss:Type=\"String\"></Data><NamedCell ss:Name=\"Print_Area\"/></Cell>\r";
  $table_header .= "   </Row>\r";
  
  // write column headers
  $table_header .= "   <Row ss:AutoFitHeight=\"1\">\r";
  foreach ($fields as $field_name => $field) {
      if (strcmp($field_name, $first_field) == 0) {
        $th_style = 's51';
      }
      elseif (strcmp($field_name, $last_field) == 0) {
        $th_style = 's53';
      }
      else {
        $th_style = 's52';
      }
      $field_label = ($field['label']) ? $field['label'] : $field_name;
      $table_header .= "   <Cell ss:StyleID=\"{$th_style}\"><Data ss:Type=\"String\">$field_label</Data><NamedCell ss:Name=\"Print_Area\"/></Cell>\r";
      
  }
  $table_header .= "   </Row>\r";
  
  // begin sheet
  $output_temp = " <Worksheet ss:Name=\"$set_label\">
  <Names>
   <NamedRange ss:Name=\"Print_Area\" ss:RefersTo=\"='$set_label'!R1C1:R{$row_count}C". $field_count ."\"/>
  </Names>
  <Table x:FullColumns=\"1\" x:FullRows=\"1\">
   <Column ss:AutoFitWidth=\"0\" ss:Width=\"100.0\" ss:Span=\"". ($field_count) ."\"/>
   <Row ss:AutoFitHeight=\"1\" ss:Height=\"25.0\">
    <Cell ss:StyleID=\"s22\" ss:MergeAcross=\"". ($field_count-1) ."\" ss:Alignment=\"Center\"><Data ss:Type=\"String\">$set_label</Data><NamedCell ss:Name=\"Print_Area\"/></Cell>
   </Row>\r". $table_header;

  // main table
  foreach ($items as $item_step => $item) {
    $output_temp .= "   <Row ss:AutoFitHeight=\"1\">\r";
    foreach ($fields as $field_name => $field) {
      switch ($field_name) {
        case($first_field):
          $td_style = 's31';
          break;
        case($last_field):
          $td_style = 's33';
          break;
        default:
          $td_style = 's32';
      }
      $cell_type = (in_array($field['type'], array('float', 'integer'))) ? 'Number' : 'String';
      $output_temp .= "   <Cell ss:StyleID=\"{$td_style}\"><Data ss:Type=\"{$cell_type}\">{$item->$field_name}</Data><NamedCell ss:Name=\"Print_Area\"/></Cell>\r";

    }
    $output_temp .= "   </Row>\r";
  }
  
  // end sheet
  $sheet_header = '&amp;LList of '. ucfirst($data_model['title']) .'&amp;C&amp;D&amp;R&amp;P of &amp;N';
  $sheet_footer = '&amp;A&amp;RPage &amp;P';
      $output_temp .= "   <Row><Cell ss:StyleID=\"s41\" ss:MergeAcross=\"". ($field_count-1) ."\"></Cell>
   </Row>\r";
    $output_temp .= "  </Table>
  <WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">
   <PageSetup>
    <Layout x:Orientation=\"Landscape\"/>
    <Header x:Data=\"{$sheet_header}\"/>
    <Footer x:Data=\"{$sheet_footer}\"/>
    <PageMargins x:Bottom=\"0.075\" x:Top=\"0.875\"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <HorizontalResolution>-4</HorizontalResolution>
    <VerticalResolution>-4</VerticalResolution>
   </Print>
   <PageLayoutZoom>0</PageLayoutZoom>
   <Selected/>
   <DoNotDisplayGridlines/>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>\r";

  // write headers and output
  header('Content-type: application/vnd.ms-excel');
  header('Content-Disposition: attachment; filename="'. $data_model['title'] .'.xls"');
  $output = '<?xml version="1.0"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:html="http://www.w3.org/TR/REC-html40"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <LastAuthor>MyTrojanUV.com</LastAuthor>
  <Created>2006-08-17T19:34:42Z</Created>
  <Version>11.518</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>21340</WindowHeight>
  <WindowWidth>28000</WindowWidth>
  <WindowTopX>-20</WindowTopX>
  <WindowTopY>-20</WindowTopY>
  <Date1904/>
  <AcceptLabelsInFormulas/>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
';
  // Now define styles
  $output .= ' <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Arial" x:Family="Roman" ss:Size="10.0"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s22">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:Family="Roman" ss:Size="16.0" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s23">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:Family="Roman" ss:Size="14.0" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s24">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:Family="Roman" ss:Size="12.0" ss:Bold="0"/>
   <Interior/>
  </Style>
  <Style ss:ID="s31">
   <Alignment ss:Horizontal="Left" ss:Vertical="Top" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:Family="Roman" ss:Size="10.0" ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous"/>
   </Borders>
  </Style>
  <Style ss:ID="s32">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:Family="Roman" ss:Size="10.0" ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous"/>
   </Borders>
  </Style>
  <Style ss:ID="s33">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:Family="Roman" ss:Size="10.0" ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous"/>
   </Borders>
  </Style>
  <Style ss:ID="s41">
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Interior/>
   <Protection/>
  </Style>
  <Style ss:ID="s51">
   <Alignment ss:Horizontal="Left" ss:Vertical="Top" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Interior ss:Color="#'. $label_color .'" ss:Pattern="Solid"/>
   <Font ss:Color="#FFFFFF" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s52">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Interior ss:Color="#'. $label_color .'" ss:Pattern="Solid"/>
   <Font ss:Color="#FFFFFF" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s53">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Interior ss:Color="#'. $label_color .'" ss:Pattern="Solid"/>
   <Font ss:Color="#FFFFFF" ss:Bold="1"/>
  </Style>
 </Styles>
';
  $output .= $output_temp;
  $output .= "</Workbook>\r";

  print $output;
  exit;
}
